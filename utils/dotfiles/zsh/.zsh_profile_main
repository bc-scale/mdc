
# system update aliases
alias update-my-system='source ~/projects/personal/scripts/bootstrap/generic/update_my_system.sh'
alias delete-lock-apt='sudo rm /var/lib/apt/lists/lock && sudo rm /var/cache/apt/archives/lock && sudo rm /var/lib/dpkg/lock'

# gpg agent related aliases
alias reload-yubikey='gpg-connect-agent "scd serialno" "learn --force" /bye'
alias reload-gpg-agent='gpg-connect-agent reloadagent /bye'
alias reload-pcsd='sudo service pcscd restart'
yubikey-reset() {
    reload-pcsd
    reload-gpg-agent
    reload-yubikey
}

# docker aliases 
alias docker-stop-containers='docker kill $(docker ps -q)'
alias docker-remove-containers='docker rm $(docker ps -a -q)'
alias docker-delete-images='docker rmi $(docker images -q)'
alias docker-delete-volumes=' docker volume ls -qf dangling=true | xargs -r docker volume rm'
alias docker-delete-all='docker ps -q | xargs docker stop ; docker system prune -a'
docker-delete-app() {
 docker rm (docker ps -a |grep "$1" |awk '{print $1}')
}

# vpn windscribe , tailscale 
wind-up() {
    windscribe connect '{print $1}'
}
wind-firewall() {
    windscribe firewall '{print $1}'
}
wind-wireguard() {
    echo " this will make tailscale unusable"
    sudo wg-quick "$1" wg."$2"
}

tail-vpn-restart() {
    sudo tailscale down
    sudo tailscale up
    sudo tailscale status
    nslookup "$1"
}

reset-dns() {
    sudo systemctl restart systemd-resolved
    sudo systemctl restart resolvconf
}

# random aliases 

search-env() {
    printenv | grep -i "$1"
}
count-lines() {
    sed '/^$/d'| awk '{print NR}'| sort -nr| sed -n '1p'
}
alias apps-killall='source ~/projects/personal/scripts/bootstrap/various_scripts/kill-relevant-apps.sh'
alias ntp-force-update='sudo date -s "$(wget -qSO- --max-redirect=0 google.com 2>&1 | grep Date: | cut -d' ' -f5-8)Z"'
alias myrsync='sudo rsync -xah --info=progress2 --no-i-r'

# uptime 
uptime-mine() {
    awk '{print int($1/3600)":"int(($1%3600)/60)":"int($1%60)}' /proc/uptime
}


# dev aliases 

# typo aliases  
alias suod='sudo'
alias rsynx='rsync'

# tmuxinator aliases 
alias mux="tmuxinator"

# git log aliases 
alias git-log="git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
alias git-author='git log --pretty=format:"%h%x09%an%x09%ad%x09%s"'

# shell aliases to search for large files/folders
alias ducks='sudo du -ckhs * | sort -rn | head'

# alias unset aws and kubeconfig 
dev-unset-vars(){
    unset AWS_PROFILE
    unset KUBECONFIG
}

# test entries 

alias myls='ls --color=tty'
alias mygrep='grep  --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn}'
alias -s {yml,yaml}=vim

# replacement commands 

alias cat='bat --style=header,grid'

alias ll="exa -Fl"
alias lll="exa -Fla"
alias l="exa -F"
alias tree="exa -TRFabghlUm"
alias ls="exa -FabghlUm"

alias myfd="fdfind"

alias cat=batcat
alias df=duf
alias man=tldr
alias du=ncdu
